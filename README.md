# joplin-markdown-table-cleaner

This is a command line utility that cleans the data in a markdown table generated from the [Joplin WebClipper Chrome extension](https://joplinapp.org/clipper/).

It takes this:

```
1![Steady](:/a2280cbc360e45e79524d6f04cff3be4 "Steady") , [David Thomson, 3rd Baron Thomson of Fleet](https://en.wikipedia.org/wiki/David_Thomson,_3rd_Baron_Thomson_of_Fleet "David Thomson, 3rd Baron Thomson of Fleet") and family , $20.10 billion ![Decrease](:/76c63e6b6529406792c4a0869821cc2f "Decrease") , ![](:/8d446aa3662041489bb2e2f4b85d202a) Ontario , [Toronto](https://en.wikipedia.org/wiki/Toronto "Toronto"), [Ontario](https://en.wikipedia.org/wiki/Ontario "Ontario") , [Thomson Reuters](https://en.wikipedia.org/wiki/Thomson_Reuters "Thomson Reuters"), [Woodbridge Co. Ltd](https://en.wikipedia.org/wiki/The_Woodbridge_Company "The Woodbridge Company") ,
```

To this:

```
Steady, David Thomson, 3rd Baron Thomson of Fleet  and family , $20.10 billion ,  Ontario , Toronto, Ontario
```

## Install

```
gem install joplin-markdown-table-cleaner
```

## Usage

```
joplin-markdown-table-cleaner my_data.csv
```


## Video of it working

TBD

## License

joplin-markdown-table-cleaner is licensed under LGPL v3.0 or later. If this doesn't work for you, reach out to me and we can work something out.
