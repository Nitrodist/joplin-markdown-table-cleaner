Gem::Specification.new do |s|
  s.name                  = 'joplin-markdown-table-cleaner'
  s.version               = '0.0.1'
  s.licenses              = ['LGPL-3.0-or-later']
  s.summary               = "Cleans the data of a CSV file generated from Joplin's WebClipper chrome extension"
  s.description           = "Take a local CSV file generated from Joplin's WebClipper chrome extension and remove the huge amount of Joplin specific Joplin specific resource links."
  s.authors               = ["Mark Campbell"]
  s.email                 = 'me@markcampbell.me'
  s.files                 = `git ls-files -- lib/*`.split("\n")
  s.executables           = `git ls-files -- exe/*`.split("\n").map{ |f| File.basename(f) }
  s.bindir                = 'exe'
  s.require_path          = "lib"
  s.homepage              = 'https://gitlab.com/Nitrodist/joplin-markdown-table-cleaner'
  s.metadata              = { "source_code_uri" => "https://gitlab.com/Nitrodist/joplin-markdown-table-cleaner" }
  s.add_development_dependency "pry", "~> 0.12"
end
